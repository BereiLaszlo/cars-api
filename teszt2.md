
VPN Split-tunnel for Windows

This article is about setting up a split-tunnel via VPN on Windows.

By default if you just turn VPN then all the traffic go through this connection, which is not efficient from the point of office bandwidth (eg youtube, downloads etc).
The goal is to separate office related traffic from personal. This can be achieved via split-tunnel which tells the machine which connection should it use for specific traffic.
The command below are similar to UNIX systems, this can be achieved there aswell by modifing the syntax.

First of all lets assume you already set up a VPN connection named Liligo and you are able to run PowerShell as administrator.

Open a PowerShell as administrator (right click – run as admin).
Get-VpnConnection
Set-VpnConnection -Name Liligo -SplitTunneling $true

First command will list VPN connections, second turn on split-tunnel on the Liligo VPN interface.
Now lets add some persistent routes. Persistent means that these routes will persist after reboot.
netsh interface ipv4 add route store=persistent 10.24.0.0/16 "Liligo"
netsh interface ipv4 add route store=persistent 10.248.0.0/16 "Liligo"
netsh interface ipv4 add route store=persistent 80.248.212.170/32 "Liligo"
netsh interface ipv4 add route store=persistent 80.248.212.210/32 "Liligo"
netsh interface ipv4 add route store=persistent 80.248.212.166/32 "Liligo"
netsh interface ipv4 add route store=persistent 10.1.0.0/16 "Liligo"
netsh interface ipv4 add route store=persistent 35.195.240.247/32 "Liligo"

    admin subnet
    legacy subnet
    par3 prod address
    par3 preprod address
    par3 admin address
    k8s etc infra subnet
    jira address

Syntax is the following:

                                 | persistent      | subnet           | VPN connection name
                                 |                 |                  |
netsh interface ipv4 add route store=persistent 80.248.212.170/32 "Liligo"

If you want to remove a route, you can easily do that by replacing the “add” command with “delete”. You can also check your manual rules with this command:
netsh interface ipv4 show route

 
